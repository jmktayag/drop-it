//
//  BezierPathView.m
//  DropIt
//
//  Created by Mckein on 12/10/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "BezierPathView.h"

@implementation BezierPathView

-(void)setPath:(UIBezierPath *)path
{
    _path = path;
    [self setNeedsDisplay]; // always call these to call draw rect
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [self.path stroke];
}


@end
