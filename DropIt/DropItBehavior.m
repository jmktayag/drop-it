//
//  DropItBehavior.m
//  DropIt
//
//  Created by Mckein on 12/10/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "DropItBehavior.h"

@interface DropItBehavior()

@property (strong, nonatomic) UIGravityBehavior *gravity; //dynamic behavior - gravity effect
@property (strong, nonatomic) UICollisionBehavior *collider; //dynamic behavior - colliding effect
@property (strong, nonatomic) UIDynamicItemBehavior *animationOptions;

@end
@implementation DropItBehavior

-(instancetype) init
{
    self = [super init];
    
    if (self){
        [self addChildBehavior:self.gravity];
        [self addChildBehavior:self.collider];
        [self addChildBehavior:self.animationOptions];
    
    }
    return self;
}

-(UIDynamicItemBehavior *)animationOptions
{
    if (!_animationOptions) {
        _animationOptions = [[UIDynamicItemBehavior alloc] init];
        _animationOptions.allowsRotation = NO; //Do not rotate blocks
    }
    return _animationOptions;
}

-(UIGravityBehavior *)gravity
{
    if (!_gravity) {
        _gravity = [[UIGravityBehavior alloc] init];
        _gravity.magnitude = 1.0;
    }
    return _gravity;
}

-(UICollisionBehavior *)collider
{
    if (!_collider) {
        _collider = [[UICollisionBehavior alloc] init];
        _collider.translatesReferenceBoundsIntoBoundary = YES; // Use the bounds of reference view as it's bounds.
    }
    
    return _collider;
}

-(void) addItem:(id<UIDynamicItem>) item
{
    [self.gravity addItem:item];
    [self.collider addItem:item];
    [self.animationOptions addItem:item];
}

-(void) removeItem:(id<UIDynamicItem>) item
{
    [self.gravity removeItem:item];
    [self.collider removeItem:item];
    [self.animationOptions removeItem:item];
}


@end
