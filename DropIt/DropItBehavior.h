//
//  DropItBehavior.h
//  DropIt
//
//  Created by Mckein on 12/10/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

//How to create custom behavior
#import <UIKit/UIKit.h>

@interface DropItBehavior : UIDynamicBehavior

-(void) addItem:(id<UIDynamicItem>) item;
-(void) removeItem:(id<UIDynamicItem>) item;
@end
