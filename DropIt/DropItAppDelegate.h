//
//  DropItAppDelegate.h
//  DropIt
//
//  Created by Mckein on 12/10/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropItAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
