//
//  BezierPathView.h
//  DropIt
//
//  Created by Mckein on 12/10/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BezierPathView : UIView

@property (strong, nonatomic) UIBezierPath *path;

@end
