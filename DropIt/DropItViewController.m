//
//  DropItViewController.m
//  DropIt
//
//  Created by Mckein on 12/10/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "DropItViewController.h"
#import "DropItBehavior.h"
#import "BezierPathView.h"

@interface DropItViewController ()<UIDynamicAnimatorDelegate> //Delegate for dynamic animator
@property (weak, nonatomic) IBOutlet BezierPathView *gameView;
@property (strong, nonatomic) UIDynamicAnimator *animator; //dynamic animator
@property (strong, nonatomic) DropItBehavior *dropItBehavior; //custom behavior
@property (strong, nonatomic) UIAttachmentBehavior *attachment; //attached two objects
@property (strong, nonatomic) UIView *droppingView;

@end

@implementation DropItViewController

static const CGSize DROP_SIZE = {40,40};


//Lazy instantiation
-(UIDynamicAnimator *)animator
{
    if (!_animator) {
        _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.gameView]; //add the reference view
        _animator.delegate = self;
    }
    return _animator;
}

#pragma mark - Dynamic Animator Delegate
-(void)dynamicAnimatorDidPause:(UIDynamicAnimator *)animator
{
    [self removeCompletedRows];
}

-(BOOL) removeCompletedRows
{
    NSMutableArray *dropsToRemove = [[NSMutableArray alloc] init];
    
    for (CGFloat y = self.gameView.bounds.size.height - DROP_SIZE.height/2; y > 0; y-=DROP_SIZE.height)
    {
        BOOL rowIsComplete = YES;
        NSMutableArray *dropsFound = [[NSMutableArray alloc] init];
        for (CGFloat x = DROP_SIZE.width/2; x<=self.gameView.bounds.size.width - DROP_SIZE.width/2; x+=DROP_SIZE.width)
        {
            UIView *hitView = [self.gameView hitTest:CGPointMake(x, y) withEvent:NULL];
            if ([hitView superview] == self.gameView){
                [dropsFound addObject:hitView];
            }else{
                rowIsComplete = NO;
                break;
            }
            
        }
        if (![dropsFound count]) break;
        if (rowIsComplete) [dropsToRemove addObjectsFromArray:dropsFound];
    }
    
    if ([dropsToRemove count]){
        for (UIView *drop in dropsToRemove){
            [self.dropItBehavior removeItem:drop];
        }
        [self animateRemovingDrops:dropsToRemove];
    }
    
    return NO;
}

-(void) animateRemovingDrops:(NSArray *) dropToRemove
{
    //Animation block
    [UIView animateWithDuration:1.0
                     animations:^{
                         for (UIView *drop in dropToRemove){
                             int x = (arc4random()%(int)self.gameView.bounds.size.width * 5 -
                                      (int)self.gameView.bounds.size.width * 2);
                             int y = self.gameView.bounds.size.height;
                             drop.center = CGPointMake(x, -y);
                         }
                     }
                     completion:^(BOOL finished) {
                         [dropToRemove makeObjectsPerformSelector:@selector(removeFromSuperview)]; // remove from superview
                     }];
}

//custom drop behavior
-(DropItBehavior *)dropItBehavior
{
    if (!_dropItBehavior) {
       _dropItBehavior = [[DropItBehavior alloc] init];
        [self.animator addBehavior:_dropItBehavior]; //add drop behavior to the animating method
    }
    return _dropItBehavior;
}

//add tap gesture to trigger the dropping of blocks
- (IBAction)tap:(UITapGestureRecognizer *)sender
{
    [self drop];
}
- (IBAction)pan:(UIPanGestureRecognizer *)sender
{
    CGPoint gesturePoint = [sender locationInView:self.gameView]; // point where you click the gesture
    if (sender.state == UIGestureRecognizerStateBegan){
        [self attachDroppingViewToPoint:gesturePoint];
    }else if (sender.state == UIGestureRecognizerStateChanged){
        self.attachment.anchorPoint = gesturePoint; // move the attachment to the anchorpoint
    }else if (sender.state == UIGestureRecognizerStateEnded){
        [self.animator removeBehavior:self.attachment];
        self.gameView.path = nil; // set the path to nil after gesture ended
    }
}

-(void) attachDroppingViewToPoint:(CGPoint)anchorPoint
{
    if (self.droppingView){
        self.attachment = [[UIAttachmentBehavior alloc] initWithItem:self.droppingView attachedToAnchor:anchorPoint]; //attach the dropping view
        UIView *droppingView = self.droppingView;
        __weak DropItViewController *weakSelf = self; //set weak to add it inside a block
        self.attachment.action = ^{
            UIBezierPath *path = [[UIBezierPath alloc] init]; //Drawing the line of attachment behavior, don't add self pointer inside block
            [path moveToPoint:weakSelf.attachment.anchorPoint];
            [path addLineToPoint:droppingView.center];
            weakSelf.gameView.path = path;
        };
        self.droppingView = nil;
        [self.animator addBehavior:self.attachment];
    }
}

//drop method for creation and animation of drop
-(void) drop
{
    CGRect frame;
    frame.origin = CGPointZero;
    frame.size = DROP_SIZE;
    int x = (arc4random()%(int)self.gameView.bounds.size.width) / DROP_SIZE.width; // Select a random number withing the bounds and align them in a grid by 40
    frame.origin.x = x * DROP_SIZE.width;
    
    UIView *dropView = [[UIView alloc] initWithFrame:frame];
    dropView.backgroundColor = [self randomColor];
    [self.gameView addSubview:dropView];
    
    self.droppingView = dropView;
    
    //add the animation
    [self.dropItBehavior addItem:dropView];
    
    
}

-(UIColor *) randomColor
{
    switch (arc4random()%5) {
        case 0: return [UIColor greenColor];
        case 1: return [UIColor blueColor];
        case 2: return [UIColor orangeColor];
        case 3: return [UIColor redColor];
        case 4: return [UIColor purpleColor];
    }
    return [UIColor blackColor];//default
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
